### Hi there 😀

### This is a small collection of commands to maintain the [Garuda](https://garudalinux.org/) operating system.

### The idea is create safe abstractions to find, install, update, and remove packages without much mental overhead.

## Installation

### Copy and paste this into your Terminal application.

```
sudo pacman -Syu base-devel reboot-arch-btw kdialog
curl -s https://codeberg.org/ShalokShalom/add-and-friends/raw/branch/main/config.fish >> $HOME/.config/fish/config.fish && exec fish
```

# **add**


```fish
add firefox
```

We don't like partial updates, so updating our system before we install something is always important.  

In order to simplify that, does `add` ensure that your system is up to date **before** it installs any software.

You can also use `add` to update your system, and not installing anything new: 

```fish
add
```

It works with all the default repo types of Garuda: **All the normal repos, AUR and Chaotic.** 

And since it always accepts the safe default answer to any interaction that pacman can come up with, do you get a very streamlined experience. 

Gone are the times where you would issue a command to go drink some tea, just to come back to the terminal to see, that the command has not even started to run, because the system waits for you to confirm some superfluous question. 🙄

And in case the system needs a reboot, we will let you know about it. 

The system will only install software once it has ensured, that itself is **fully up to date.**


# find

```fish
search firefox
```

By default will this show you all the packages, that contain the search term in either the name or the description.  

If it finds none of that, will the command show you all the file names that match. 

This is particulary useful for people who create new packages.  

The search results will be sorted in a sensible manner, and exact name hits will be prominently shown as well.  

It also looks up if a package is already installed, and displays then some additional details about it.


# remove

```fish
remove firefox
```

Does remove files safely.  

Makes reasonable defaults about the handling of dependencies.  


# commit

```fish
commit "This is the commit message"
```

This is a neat bonus for people who interact with git. 
It will commit all the changes that you did to a cloned repo in one sweep.

Inspect the source code for details ;)


## Thanks to the maintainer of fish, who does a wonderful job and is always of great help :raised_hands:
