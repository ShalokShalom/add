# Abstractions for increased readability:

function no_error_output
   $argv 2> /dev/null
end


function argument_is_provided
   set -q argv[1]
end


function command_not_successful
   test $status -eq 1
end


function newline
   echo ""
end


function log_update
   echo [(date +"%Y-%m-%dT%T%z")] [FISH] System is updated | sudo tee -a /var/log/pacman.log >/dev/null
end


function updated
   test 2400 -ge (path mtime --relative /var/log/pacman.log) && # Prevents that updates run even if the system has been updated recently.
   string match -rq "System is updated" (tail -2 /var/log/pacman.log) # Prevents that canceled updates count as complete updates.
end


# add-and-friends

function add --wraps "paru -S"

   if not updated
      update --skip-mirrorlist --noconfirm

      if command_not_successful
         update --skip-mirrorlist
      end

      paru -Sua --skipreview --useask --noconfirm  &&
      sudo pkgfile --update && log_update
         
      if string match -q "*eboot*" (reboot-arch-btw --verbose)
         reboot_after_update reboot
      else if string match -q "*estart*" (reboot-arch-btw --verbose)
         reboot_after_update restart
      end
      
      add $argv

   else

   newline

   set_color green && echo "System is up to date."
   set_color normal

   newline

      if argument_is_provided $argv
         no_error_output sudo pacman -S --noconfirm $argv && log_update

         if command_not_successful
            no_error_output paru -S --aur --skipreview --useask --noconfirm $argv && log_update

            if command_not_successful
               echo ":: Program was not found.." && log_update
               read -p 'set_color green; echo -n "$prompt Do you like to look for programs with a similar name? [Y/n]: "; set_color normal' -l confirm

               switch $confirm

               case Y y ''
                  search $argv

               case N n
                  return

               end
            end
         end
      end
   end
end


function search --wraps "paru -Ss"
   
   set -l success no
   
   paru -Ss --aur $argv; and set success yes; and newline
   pacman -Ss $argv; and set success yes; and newline
   no_error_output pacman -Qi $argv; and set success yes

   if test $success = no
      read -p 'set_color green; echo -n "$prompt Do you like to look for files that are contained in packages? [Y/n]: "; set_color normal' -l confirm

      switch $confirm

      case Y y '' 
         pkgfile -vri $argv

         if command_not_successful
            set_color blue
            echo " No package file with that name found in the available repos."
            set_color normal
         end  

      case N n
         return

      end
   end
end


function remove --wraps "pacman -Rdd"
   
   sudo pacman -Rdd $argv

   if command_not_successful
      sudo pacman -Runs $argv

      if command_not_successful
         sudo pacman -Rcns $argv

      end
   end
end


function commit

   if argument_is_provided $argv
      git add .
      git commit -am "$argv"
      git push

   else

      newline
      echo "Please provide a commit message:" && newline
      set_color blue
      printf "commit "
      set_color green
      printf "\"this is a commit message\""
      set_color normal

   end
end


function reboot_after_update $argv

   switch $argv
   case reboot

      set choice (kdialog --title "System Update" --default "Yes, reboot now" --combobox " <div style='text-align: center; font-size: 15px; line-height: 1;'> <span style='color: green;'> <br>

      Reboot after update <br> <br> 
      &nbsp; The update that was performed, needs a reboot to finish. &nbsp; </span> <br> <br> 
      Choose to reboot now, or let me remind you a bit later: <br> </div>" "Yes, reboot now" "Remind me 10 minutes later" "Remind me 20 minutes later" "Remind me 40 minutes later" "Remind me 60 minutes later" "Shutdown")

      if test "$choice" = "Yes, reboot now"
         reboot

      else if string match -q "Remind me*" "$choice"
         set clean (string replace 'Remind me ' '' $choice)
         set cleaned (string replace ' minutes later' '' $clean)
         fish -c "sleep (math $cleaned \* 60) && reboot_after_update reboot" &

      else if test "$choice" = "Shutdown"
         shutdown now

      else # Cancel the dialog
         fish -c "sleep 3600 && reboot_after_update reboot" &
      end

   case restart

      set choice (kdialog --title "System Update" --default "Yes, reload desktop session" --combobox " <div style='text-align: center; font-size: 15px; line-height: 1;'> <span style='color: green;'> <br>

      Reload session after update <br> <br> 
      &nbsp; The update that was performed, needs to reload the session to finish. &nbsp; </span> <br> <br> 
      Choose to reload the session now, or let me remind you a bit later: <br> </div>" "Yes, reload desktop session" "Remind me 10 minutes later" "Remind me 20 minutes later" "Remind me 40 minutes later" "Remind me 60 minutes later" "Shutdown")

      if test "$choice" = "Yes, reload desktop session"
         plasmashell --replace

      else if string match -q "Remind me*" "$choice"
         set clean (string replace 'Remind me ' '' $choice)
         set cleaned (string replace ' minutes later' '' $clean)
         fish -c "sleep (math $cleaned \* 60) && reboot_after_update reboot" &

      else if test "$choice" = "Shutdown"
         shutdown now

      else # Cancel the dialog
         fish -c "sleep 3600 && reboot_after_update reboot" &

      end
   end
end